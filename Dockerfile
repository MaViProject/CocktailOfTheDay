FROM python:3-slim-buster
WORKDIR /var/cotd
VOLUME /var/cotd
COPY requirements.txt .
COPY License .
COPY README.MD .
COPY .env .
COPY *.py ./
COPY cocktails/* ./cocktails/
COPY telegram/* ./telegram/
COPY helpers/* ./helpers/
RUN pip3 install -r requirements.txt
ENV FLASK_ENV=production, FLASK_APP=app.py
CMD ["flask", "run", "--host", "0.0.0.0"]