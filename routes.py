#!/usr/bin/env python3

from cocktails.cocktails import GetRandomCocktail, GetCocktail, GetCocktailList
#from cocktails.probes import ReadinessProbe, HealthProbe



def initialize_cocktail_routes(api):
  api.add_resource(GetRandomCocktail, '/getRandomCocktail')
  api.add_resource(GetCocktail, '/getCocktail/<drink>')
  api.add_resource(GetCocktailList, '/getCocktailList/<ingredient>')

  
'''
def initialize_probe_routes(api):
  api.add_resource(ReadinessProbe, '/ready')
  api.add_resource(HealthProbe, '/health')
'''