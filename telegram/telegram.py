#!/usr/bin/env python3

'''
Deals with api.telegram.org API operations.
A bot has been created to send message to a single user
- polls the bot for updates
- parse the update results filtering today's requests and maintaining the offset for the already requested
- sends message to the bot
'''
from flask import jsonify
import requests
import json
from datetime import datetime as dt, date 
from random import randrange
import settings
import helpers.logger as log
import helpers.standard_returns_msgs as rmsg



logger = log.setlog("telegram.log")
#modelfile = settings.telegram_model
offset = 0


# Send a message to the Telegram bot
def SendCocktail(ck):
  logger.debug("sendCocktail - Sending the "+str(ck['drink'])+" cocktail to telegram ")
  photo = "photo="+ck['img']
  caption = "caption="+ck['drink']
  chatId = ck['dest']
  message = '**' + ck['drink']+'**\n'
  for ing in ck['ingredients']:
    message = message + "- " + ing + '\n'
  procedure = ""
  if ck['prep_it']:
    procedure = ck['prep_it']
  else :
    procedure = ck['prep_en']
  message = message + procedure + '\n'
  body = 'text='+message
  chat_id = "chat_id="+str(chatId)
  send_photo = settings.tg_domain + settings.tg_api + settings.tg_bot_token + "/sendPhoto?"+chat_id+'&'+photo+'&'+caption
  send_howto = settings.tg_domain + settings.tg_api + settings.tg_bot_token + "/sendMessage?"+chat_id+'&'+body
  try: 
    resp1 = requests.post(send_photo)
    resp2 = requests.post(send_howto)
    if not resp1 or not resp2:
      logger.error("sendCocktail - An error occurred while sending caption "+str(resp1)+" or message: "+str(resp2)+".")
      return jsonify(rmsg.m_404)
    return jsonify(rmsg.m_200)
  except Exception as e:
    logger.error("sendCocktail - An error occurred while sending your cocktail to Telegram: "+str(e))
    return jsonify(rmsg.m_500)


# Send a message listing the commands
def SendCommands(chatId):
  logger.debug("SendCommands - Command list has been required.")
  cmds = 'text='+"Scrivi /commands oppure /start oppure /help per avere questa lista di comandi. "
  cmds = cmds + "Scrivi /drink seguito dal nome del drink desiderato per avere la preparazione (e.g.: /drink bloody mary). "
  cmds = cmds + "Scrivi /ingredient seguito dal nome del singolo ingredeinte desiderato per avere la preparazione di un cocktail che lo contenga (e.g.: /ingredient vodka). "
  cmds = cmds + "Scrivi /random per avere la preparazione di un cocktail preso a caso dal DB (e.g.: /random). "

  chat_id = "chat_id="+str(chatId)
  send_cmds = settings.tg_domain + settings.tg_api + settings.tg_bot_token + "/sendMessage?"+chat_id+'&'+cmds

  try: 
    resp = requests.post(send_cmds)
    return jsonify(rmsg.m_200)
  except Exception as e:
    logger.error("SendCommands - An error occurred while sending command instructions to Telegram: "+str(e))
    return jsonify(rmsg.m_500)


# Poll the Telegram bot for updates and return the array
def GetUpdates():
  global offset
  logger.debug("GetUpdates - Searching new requests in telegram with offset "+str(offset))
  get_updates = settings.tg_domain + settings.tg_api + settings.tg_bot_token + "/getUpdates?offset="+str(offset)
  try: 
    resp = requests.get(get_updates).content
    if not resp:
      return(jsonify(rmsg.m_404))
    resp = json.loads(resp.decode('utf-8'))  
    if resp['ok']:
      logger.debug("GetUpdates - Updates have been succesfully retrieved, returning the results")
      return {
        "status": 200,
        "message": resp['result']
      }
    else:
      return (jsonify(rmsg.m_503))
  except Exception as e:
    logger.error("GetUpdates - An error occurred while getting updates from Telegram: "+str(e))
    return(jsonify(rmsg.m_500))


# Create the request array to send to cocktail DB
def ExtractMessages(requests):
  global offset
  to_send = []
  for req in requests:
    msg_date = dt.utcfromtimestamp(int(req['message']['date'])).strftime('%Y%m%d')
    today = date.today().strftime('%Y%m%d')
    # only today's requests
    if msg_date == today:
      if req['message']['entities'] and req['message']['entities'][0]['type']=='bot_command':
        # only 1 split to separate the command from the name (which can contain spaces): max 2 args
        command = req['message']['text'].split(" ",1)
        if len(command)==2:
          if command[0] == "/drink":
            to_send.append({
              "dest": req['message']['chat']['id'],
              "type": "cocktail",
              "cmd": command[0],
              "param": command[1]
            })
           
          elif command[0] == "/ingredient":
            to_send.append({
              "dest": req['message']['chat']['id'],
              "type": "list",
              "cmd": command[0],
              "param": command[1]
            })
          else:
            logger.error("ExtractMessages - Invalid command: "+str(command)+". Pass.")
            pass            
        
        # only random cocktail and command request call have 1 argument
        else: 
          if command[0] == "/random":
            # call the cocktail DB API for a random drink
            to_send.append({
              "dest": req['message']['chat']['id'],
              "type": "cocktail",
              "cmd": command[0],
              "param": False
            })

          elif command[0] == "/help" or command[0] == "/start" or command[0] == "/commands":
            # return indication to send default commands message
            to_send.append({
              "dest": req['message']['chat']['id'],
              "type": "commands",
              "cmd": "commands",
              "param": False
            })
            
          else:
            logger.error("ExtractMessages - Command without valid arguments: "+str(command)+". Pass.")
            pass
        
        # in any of these case update the offset for the next update
        offset = req['update_id']+1
      else:
        pass
    else:
      pass
    
  # multiple possible messages to send
  if not to_send:
    logger.debug("ExtractMessages - The request did not return the expected value.")
    pass
  else:
    logger.debug("ExtractMessages - Returning the array of request messages to be sent to Telegram.")
    return to_send
      
