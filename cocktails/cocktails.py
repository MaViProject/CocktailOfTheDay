#!/usr/bin/env python3

'''
Deals with www.thecocktaildb.com API operations.
- gets the random cocktail
- gets a list of cocktails based on an ingredient
- gets a cocktail by name
'''
from flask import jsonify
from flask_restful import Resource
import requests
import json

import settings
import helpers.logger as log
import helpers.standard_returns_msgs as rmsg


logger = log.setlog("cocktails.log")
#modelfile = settings.cocktail_model

# retrieve a random cocktail
class GetRandomCocktail(Resource):
  def get(self):
    logger.debug("getRandomCocktail - Random cocktail requested")
    site_request = settings.ck_domain + settings.ck_api + settings.ck_api_version + "random.php"
    try: 
      resp = requests.get(site_request).content
      if not resp:
        return(jsonify(rmsg.m_404))
      cocktails = json.loads(resp.decode('utf-8'))  
      logger.debug("getRandomCocktail - Random cocktail has been returned by cocktaildb.")    
      cocktail = formatCocktail(cocktails)
      return {
        "status": 200,
        "message": cocktail
      }

    except Exception as e:
      logger.error("getRandomCocktail - An error occurred while requesting your cocktail: "+str(e))
      return(jsonify(rmsg.m_500))
  

# retrieve the cocktail basing on the name
class GetCocktail(Resource):
  def get(self,drink):
    logger.debug("GetCocktail - "+ str(drink) +" cocktail has been requested")
    site_request = settings.ck_domain + settings.ck_api + settings.ck_api_version + "search.php?s="+str(drink)
    try: 
      resp = requests.get(site_request).content
      if not resp:
        return(jsonify(rmsg.m_404))
      cocktails = json.loads(resp.decode('utf-8'))  
      logger.debug("GetCocktail - "+ str(drink) +" has been returned by cocktaildb.")    
      cocktail = formatCocktail(cocktails)
      return {
        "status": 200,
        "message": cocktail
      }

    except Exception as e:
      logger.error("GetCocktail - An error occurred while requesting your cocktail: "+str(e))
      return(jsonify(rmsg.m_500))
  

# retrieve the list of cocktails having the desidered ingredient
class GetCocktailList(Resource):
  def get(self,ingredient):
    logger.debug("GetCocktailList - Cocktails with "+ str(ingredient) +" have been requested")
    site_request = settings.ck_domain + settings.ck_api + settings.ck_api_version + "filter.php?i="+str(ingredient).lower()
    try: 
      resp = requests.get(site_request).content
      if not resp:
        return(jsonify(rmsg.m_404))
      cocktails = json.loads(resp.decode('utf-8'))  
      logger.debug("GetCocktailList - A list of cocktails containing "+str(ingredient)+" has been returned by cocktaildb.")    
      cocktail_lst = formatCocktailList(cocktails)
      return {
        "status": 200,
        "message": cocktail_lst
      }

    except Exception as e:
      logger.error("GetCocktailList - An error occurred while requesting your cocktail: "+str(e))
      return(jsonify(rmsg.m_500))


## Internal Functions

# return the cocktail information
def formatCocktail(cocktails):
  c = cocktails['drinks'][0]
  ingredients = []
  # the return message has a max nr of 16 ingredients
  for ndx in range(1,16):
    if c['strIngredient'+str(ndx)]:
      ingredients.append(str(c['strIngredient'+str(ndx)])+" "+str(c['strMeasure'+str(ndx)]))
  cocktail = {
    "drink": c['strDrink'],
    "category": c['strCategory'],
    "glass": c['strGlass'],
    "alcoholic": c['strAlcoholic'],
    "ingredients": ingredients,
    "prep_en": c['strInstructions'],
    "prep_it": c['strInstructionsIT'],
    "img": c['strDrinkThumb']
  }
  return cocktail


# return the list of cocktails 
def formatCocktailList(clist):
  cocktails = []
  for c in clist['drinks']:
    cocktails.append({
      "drink": c['strDrink'],
      "img": c['strDrinkThumb']
    })
  return cocktails
