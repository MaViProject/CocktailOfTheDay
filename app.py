#!/usr/bin/env python3

import settings
from flask import Flask
from flask_restful import Api
from routes import  initialize_cocktail_routes
from apscheduler.schedulers.background import BackgroundScheduler as bsched
from random import randrange
import requests
import json
import telegram.telegram as tg
import cocktails.cocktails as ck
import helpers.logger as log

logger = log.setlog("main.log")


app = Flask(__name__)
api = Api(app)

get_cklst = ck.GetCocktailList()
get_ck = ck.GetCocktail()
get_rck = ck.GetRandomCocktail()


# for testing purposes
@app.route("/")
def helloWorld():
  return "Hello, service is up and running"

initialize_cocktail_routes(api)
#initialize_telegram_routes(api)


# Main tasks to execute periodically
def job_function():
  logger.debug('job_function - In cron job')
  
  # get the new requests from telegram
  upds = tg.GetUpdates()
  if upds['status'] == 200:
    logger.debug('job_function - Updates retrieved by Telegram will be processed')
    to_send = tg.ExtractMessages(upds['message'])
    #print(to_send)
    if to_send:
      # retrieve the cocktail from cocktaildb for each request then add the destination to the response and forward to telegram
      with app.app_context():
        for creq in to_send:
          resp = {}
          # Random drink by ingredient
          if creq['type']=='list' and creq['cmd']=='/ingredient':
            clst = get_cklst.get(creq['param'])
            if clst['status']==200:
              selected_cocktail = randrange(0,len(clst['message'])-1)
              ckl = clst['message'][selected_cocktail]
              resp = get_ck.get(ckl['drink'])

          # Drink by name  
          elif creq['type']=='cocktail' and creq['cmd']=='/drink' and creq['param']:
            resp = get_ck.get(creq['param'])

          # Random drink
          elif creq['type']=='cocktail' and creq['cmd']=='/random' and not creq['param']:
            resp = get_rck.get()

          # Request for command to use
          elif creq['type']=='commands':
            resp = {
              'status': 300,
              'message': 'commands'
            }

          # Other not managed cases 
          else:
            resp = {
              'status': 404,
              'message': 'Not found'
            }
            pass # to the next request in the for
          

          # Finally send the drink
          if resp['status'] == 200:
            resp['message']['dest'] = creq['dest']
            res = tg.SendCocktail(resp['message'])
          
          elif resp['status'] == 300:
            res = tg.SendCommands(creq['dest'])
          
          else:
            logger.error("job_function - Request combination didn't match.")
            pass

      
        #return (jsonify(rmsg.m_200))

  else:
    logger.error("job_function - Error while querying the telegram updates.")
    pass
    #return(jsonify(rmsg.m_404))
  

# Scheduler for task triggering 
cron = bsched(daemon=True)
cron.add_job(func=job_function, trigger="interval", seconds=settings.polling_s)
cron.start()




if __name__=="__main__":
  app.run(host='0.0.0.0',port=settings.port,debug=False)

    
