from os import environ 

# GENERIC
port = environ.get('PORT')
polling_s = 10

# LOGGING
logfolder = "./logs/"
loglevel = "DEBUG"

# TELEGRAM
tg_domain = "https://api.telegram.org/"
tg_api = "bot"
tg_bot_token = environ.get('BOT_TOKEN')

# Cocktail DB
ck_domain = "https://www.thecocktaildb.com/"
ck_api = "api/json/"
ck_api_version = "v1/1/"