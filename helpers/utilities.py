#!/usr/bin/env python3
import zipfile
import os
import shutil
import helpers.logger as log

logger = log.setlog("utilities.log")
'''
def zipdir(path, ziph):
    # ziph is zipfile handle
    for root, dirs, files in os.walk(path):
        for file in files:
            ziph.write(os.path.join(root, file))
'''

def sanitizeString(input_string):
  logger.debug("sanitizeString - Sanitizing the string... ")
  output_string = ''
  for i in input_string:
    if i == '>':
      outchar = '&gt;'
    elif i == '<':
      outchar = '&lt;'
    elif i == '!':
      outchar = '&#33;'
    elif i == '"':
      outchar = '&#34;'   
    elif i == '$':
      outchar = '&#35;'
    elif i == '$':
      outchar = '&#36;'
    elif i == '%':
      outchar = '&#37;'
    elif i == '&':
      outchar = '&#38;'
    elif i == "'":
      outchar = '&#39;'
    elif i == '*':
      outchar = '&#42;'
    elif i == '?':
      outchar = '&#63;'
    elif i == '@':
      outchar = '&#64;'   
    else:
      outchar = i
      output_string += outchar
  logger.debug("sanitizeString - The sanitized string is: "+output_string)
  return output_string

# check the filetype
def checkFileType(allowed, type):
  logger.debug("checkFileType - Checking if the file type "+type+" is allowed")
  for atype in allowed:
    if atype.lower() == type.lower():
      return True
  return False

def checkFileSize(maxSize, size):
  logger.debug("checkFileSize - Checking if the file size "+str(size)+" is allowed")
  if maxSize >= size:
    return True
  return False
