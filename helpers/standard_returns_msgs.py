'''
Contains the standard messages to return basing on HTML codes. 
'''

# 2xx - success
m_200 = {
    "status": 200,
    "message": "OK"
}

m_201 = {
    "status": 201,
    "message": "Resource has been created"
}


# 4xx - errors
m_400 = {
    "status": 400,
    "message": "Bad Request"
}

m_401 = {
    "status": 401,
    "message": "Unauthorized"
} 

m_402 = {
    "status": 402,
    "message": "Optimistic Lock"
} 

m_403 = {
    "status": 403,
    "message": "Forbidden"
} 

m_404 = {
    "status": 404,
    "message": "Not Found"
} 


# 5xx - server errors
m_500 = {
    "status": 500,
    "message": "Internal server error"
} 

m_503 = {
    "status": 503,
    "message": "Service Unavailable"
} 
