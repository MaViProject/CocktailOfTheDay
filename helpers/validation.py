#!/usr/bin/env python3

import re 
import json
import helpers.logger as log
import settings

logger = log.setlog("validation.log")


#gets the posted data as array and according to the resource performs the validation
def validate(data, resource, modelfile):
    logger.debug("validate - function called")

    # gets the model (how the json must be structured) for this resource
    model = getModel(resource, modelfile)
    if not model:
        logger.error("validate - Impossible to retrieve the JSON model for "+ resource + ". This is a cascaded error.")
        return False
    
    # checks if data has mandatory fields
    mandatory_satisfied = checkMandatoryFields(data, model)
    if not mandatory_satisfied:
        logger.error("validate - Some mandatory field has not been filled in the request for "+ resource +". This is a cascaded error.")
        return False
    
    # checks if the json has more fields than the model
    has_extra_fields = checkExtraFields(data, model)
    if has_extra_fields:
        logger.error("validate - The JSON has more field than allowed by the model. This is a cascaded error.")
        return False
    
    # checks if the restriction specified in the model are respected
    restrictions_respected = validateModel(data, model)
    if not restrictions_respected:
        logger.error("validate - Model's restriction not respected. This is a cascaded error.")
        return False
  


    logger.debug("validate - JSON model retrieved for " + resource + " go ahead with the validation.")
    return True


# data sanification
def sanitizeInput(data):
    pass

#return the right model for the passed
def getModel(resource, modelFile):
    logger.debug("getModel - function called for the resource " + resource)
    try:
        with open(modelFile, 'r') as json_file:
            model = json_file.read()
        res = json.loads(model)
        logger.debug("getModel - The model for "+ resource + " has been succesfully retrieved.")
        #logger.debug(res[resource])
        return res[resource]
    except:
        logger.error("getModel - An error occurred while opening the model.json" )
        return False
    
    


#check the data type for particular data
def validateFormat(message, format, model):
    if format == "email":
        logger.debug("validateFormat - Validating an email")
        return checkEmail(message)
    elif format == "password":
        logger.debug("validateFormat - Validating a password")
        #logger.debug(message)
        return checkPassword(message)
    elif format == "list":
        logger.debug("validateFormat - Validating a list: ")
        if isinstance(message, list):
            return checkList(message, model)
        else: 
            logger.error("validateFormat - expected value was a list.")
            return False
    elif format == "dictionary":
        logger.debug("validateFormat - Validating a dictionary: ")
        return checkDictionary(message, model)
    else:
        return True
    
    '''
    elif isinstance(message, format):
        logger.debug("validateFormat - The field "+ message + "has the expected type")
        return True
    else:
        logger.error("validateFormat - The field " + message + " has not the expected type")
        return False
   '''

#check if the dictionary is correct
def checkDictionary(dic, model):
    logger.debug("checkDictionary - The validating item is a dictionary")
    if not "dictref" in dic:
        logger.error("checkDictionary - The passed item has no reference structure in the model")
        return False
    reference = dic["dictref"]
    logger.debug("checkDictionary - Call the main function on a subset using " + reference + " as action in the model")
    return validate(dic, reference, model)


#check if email has correct format
def checkEmail(email):  
    # Make a regular expression for validating an Email 
    regex = r"^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$"
    # pass the regualar expression and the string in search() method 
    if re.search(regex, email):  
        logger.debug("checkEmail - email "+ email +" has been succesfully validated.")
        return True  
    else:  
        logger.error("checkEmail - "+ email +" is not a valid email.")
        return False  

#check password strenght
def checkPassword(password):
    regex = r"[@_!#$%^&*()<>?/\|}{~:]"

    has_upper = False
    has_lower = False
    has_numbers = False
    has_symbols = False
    has_length = False

    # has length
    if len(password) >= 8:
        has_length = True
    # has upper
    if re.search(r'[A-Z]', password):
        has_upper = True
    # has lower
    if re.search(r'[a-z]', password):
        has_lower = True
    # has numbers
    if re.search(r'[0-9]', password):
        has_numbers = True
    # has symbols
    if(re.search(regex, password)):  
        has_symbols = True 
    
    result = has_upper and has_lower and has_numbers and has_symbols and has_length
    if result:
        logger.debug("checkPassword - "+ password +" is a valid password.")
    else:
        logger.error("checkPassword - "+ password +" is too week as a password.")
    return result

#check if list has duplicates
def checkList(listeddt, model):
    unique = []
    for item in listeddt:
        if isinstance(item, dict):
            #list contains a dictionary: check dictionary
            logger.debug("checkList - The list contain a dictionary, call the checkDictionary for this item")
            return checkDictionary(item, model)
        if item not in unique:
            unique.append(item)
        else: 
            logger.error("checkList - "+ item +" appears several times in the list.")
            return False
    logger.debug("checkList - The list has no duplicates.")
    return True

#check if there is some other field than the model's one
def checkExtraFields(data, model):
    modelitems = []
    dataitems = []
    for item in model:
        modelitems.append(item)
    for item in data:
        dataitems.append(item)
    modelitems.sort()
    dataitems.sort()
    if dataitems == modelitems:
        logger.debug("checkExtraFields - JSON and model match")
        return False
    logger.error("checkExtraFields - JSON has different fields than model")
    return True


#gets the mandatory fields from the model and checks if all of them are filled
def checkMandatoryFields(data, model):
    mandatory = []
    for item in model:
        req = model[item]
        if req["required"]:
            mandatory.append(item)
    if len(mandatory) > 0 :
        for field in mandatory:
            if field in data:
                logger.debug("checkMandatoryFields - found " + field + " in posted data.")
            else:
                logger.error("checkMandatoryFields - " + field + " not found in posted data.")
                return False
    
    return True

#for each field in the model checks if the requirements are verified for each field of the JSON
def validateModel(data, model):
    for item in model:
        field = model[item]
        u_limit = False
        d_limit = False
        allowed = False
        typ = False
        if "max_length" in field:
            u_limit = field["max_length"]
            logger.debug("validateModel - " + item + " has a max length set")
        if "min_length" in field:
            d_limit = field["min_length"]
            logger.debug("validateModel - " + item + " has a min length set")
        if "allowed" in field:
            allowed = field["allowed"]
            logger.debug("validateModel - " + item + " has a restricted set of possible values.")
        if "type" in field:
            typ = field["type"]
            logger.debug("validateModel - " + item + " has a defined type.")
        for dt in data:
            if dt == item:
                if u_limit and len(data[dt]) > u_limit:
                    logger.error("validateModel - " + dt + " field does not respect the max length limit")
                    return False
                if d_limit and len(data[dt]) < d_limit:
                    logger.error("validateModel - " + dt + " field does not respect the min length limit")
                    return False
                if allowed: 
                    for element in data[dt]:
                        if element not in allowed:
                            logger.error("validateModel - " + element + " is not an allowed field in " + dt)
                            return False
                if typ:
                    valid_format = validateFormat(data[dt], typ, model)   
                    if not valid_format: 
                        logger.error("validateModel - " + dt + " has no valid format")
                        return False
                
    return True

